<?php
/**
 * Footer
 */
?>

<footer>
    <?php $footer_options = get_field('footer_options', 'option')[0]; ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'inline-list','fallback_cb' => 'foundation_page_menu')); ?>
            </div>
        </div>

        <?php if($footer_options['copyright_options']){ ?>
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $footer_options['copyright_options']; ?>
                </div>
            </div>
        <?php }?>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>