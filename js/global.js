;(function ($) {

$(document).ready(function() {
    /* -----------------------------------------
   JavaScript on media query breakpoints
----------------------------------------- */
document.body.addEventListener('breakpoint', function(e){
    switch(e.detail.lastBreakpoint){
        case 'xs':
            //Add scripts for small screens here:
            
        break;
        case 'sm':
            // Add scripts for medium screens here:

        break;
        case 'md':
            // Add scripts for large screens here:

        break;
        case 'lg':
            // Add scripts for large screens here:

        break;
    }
});

try{
    var responsiveJs = new CustomEvent('breakpoint', {
        detail : {
            utilityElem: window.getComputedStyle(document.body, ':before'),
            lastBreakpoint:""
        },
        bubbles : false,
        cancelable : false
    });
} catch(er){
    var responsiveJs = document.createEvent('CustomEvent');
    responsiveJs.initCustomEvent('breakpoint', false, false, {
        utilityElem: window.getComputedStyle(document.body, ':before'),
        lastBreakpoint:""
    });
}
responsiveJs.selfDispatching = function(){
    var eventData = this.detail,
    currentBreakpoint = eventData.utilityElem.getPropertyValue('content').replace(/['"]+/g, '');
    if(eventData.lastBreakpoint !== currentBreakpoint){
        eventData.lastBreakpoint = currentBreakpoint;
        document.body.dispatchEvent(this);
    }
};

window.addEventListener('load', function(){
    responsiveJs.selfDispatching();
});
window.addEventListener('resize', function(){
    responsiveJs.selfDispatching();
});
window.addEventListener('orientationchange', function(){
    responsiveJs.selfDispatching();
});





    //Remove placeholder on click
    $("input,textarea").each(function() {
	    $(this).data('holder',$(this).attr('placeholder'));
        $(this).focusin(function(){
            $(this).attr('placeholder','');
	    });
        $(this).focusout(function(){
            $(this).attr('placeholder',$(this).data('holder'));
        });
    });


});

$(window).load(function() {


});

}(jQuery));