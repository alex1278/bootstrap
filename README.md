# TWLA Bootstrap boilerplate theme #

This is main dev repository of Bootstrap theme.
It synced with theme installed on [http://ready-for-feedback.com/development/bs-starter-kit/](http://ready-for-feedback.com/development/bs-starter-kit/). Use Duplicator to deploy Starter kit when create new project.

To install the theme separately follow next steps:


```
#!git

ssh twla@192.168.1.8
pw: **************************
cd /home/www/{{CUSTOMER_NAME}}/{{PROJECT_NAME}}/wp-content/themes/
git clone https://bitbucket.org/twladevelopment/bootstrap.git theme-name
```